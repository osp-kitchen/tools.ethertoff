from django.apps import AppConfig


class EthertoffRdfindexConfig(AppConfig):
    name = 'ethertoff_rdfindex'
